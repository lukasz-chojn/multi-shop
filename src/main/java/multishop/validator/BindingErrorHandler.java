package multishop.validator;

import lombok.extern.slf4j.*;
import multishop.exception.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.stereotype.*;
import org.springframework.validation.*;

import java.util.*;
import java.util.stream.*;

/**
 * Klasa walidująca obiekty dto
 */
@Component
@Slf4j
public class BindingErrorHandler {

    public void checkForErrors(BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {

            List<String> errors = bindingResult.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());
            log.error(errors.toString());

            throw new RequestValidationException(errors.toString(), HttpStatus.BAD_REQUEST);
        }
    }
}
