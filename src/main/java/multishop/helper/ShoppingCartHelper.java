package multishop.helper;

import multishop.dto.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.service.*;
import org.json.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import java.math.BigDecimal;
import java.util.*;

/**
 * Klasa pomocnicza do walidacji koszyka
 */
@Component
public class ShoppingCartHelper {

    private ProductRepository productRepository;
    private ProductService productService;
    private AssortmentHelper assortmentHelper;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setAssortmentHelper(AssortmentHelper assortmentHelper) {
        this.assortmentHelper = assortmentHelper;
    }

    public ShoppingCartClientDto totalCostsShoppingCart(ShoppingCartDto shoppingCartDto) {
        ShoppingCartClientDto shoppingCartClientDto = new ShoppingCartClientDto();
        List<Product> products = new ArrayList<>();
        BigDecimal totalCosts = BigDecimal.ZERO;

        JSONObject jsonObject = new JSONObject(shoppingCartDto);
        JSONArray jsonArray = jsonObject.getJSONArray("productList");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            JSONObject assortment = obj.getJSONObject("assortment");

            Product p = productRepository.findByName(obj.get("name").toString());
            int quantity = Integer.parseInt(assortment.get("quantity").toString());
            Integer amount = productService.availableAmount(p, quantity);

            if (productService.availableAmount(p, quantity) <= 0) {
                continue;
            }
            products.add(p);
            totalCosts = totalCosts.add(productService.totalCosts(p, quantity));
            assortmentHelper.update(p, quantity, amount);
        }
        shoppingCartClientDto.setProductList(products);
        shoppingCartClientDto.setTotalPriceOfProducts(totalCosts);

        return shoppingCartClientDto;
    }
}
