package multishop.helper;

import multishop.model.*;
import multishop.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.transaction.annotation.*;

/**
 * Klasa agregująca operacje dla kontrolera Assortment
 */
@Component
public class AssortmentHelper {

    private AssortmentRepository assortmentRepository;

    @Autowired
    public void setAssortmentRepository(AssortmentRepository assortmentRepository) {
        this.assortmentRepository = assortmentRepository;
    }

    @Transactional
    public void update(Product product, Integer quantity, Integer sellQuantity) {
        assortmentRepository.updateQuantity(product.getId(), quantity);
        assortmentRepository.updateSellQuantity(product.getId(), sellQuantity);
    }
}
