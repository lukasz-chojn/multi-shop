package multishop.helper;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import multishop.config.constants.*;
import multishop.model.*;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.messaging.simp.*;
import org.springframework.stereotype.*;

/**
 * Klasa wspomagająca wysyłkę wiadomości na kolejkę
 */
@Component
public class MessageBuilderHelper {
    private RabbitTemplate rabbitTemplate;
    private ObjectMapper objectMapper;

    @Autowired
    public void setRabbitTemplate(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public Message buildMessage(Product product) throws JsonProcessingException {
        String productToQueue = objectMapper.writeValueAsString(product);

        Message productMessage = MessageBuilder
                .withBody(productToQueue.getBytes())
                .setHeader(SimpMessageHeaderAccessor.MESSAGE_TYPE_HEADER, productToQueue)
                .setContentType(MessageProperties.CONTENT_TYPE_JSON)
                .build();

        rabbitTemplate.convertAndSend(RabbitConstant.ADD_PRODUCT, productMessage);
        return productMessage;
    }
}