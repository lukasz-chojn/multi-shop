package multishop.helper;

import multishop.dto.*;
import multishop.model.*;
import multishop.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;

/**
 * Klasa weryfikująca istnienie produktu w bazie
 */
@Component
public class ProductHelper {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Product getExistingProduct(@RequestBody @Valid ProductDto productDto) {
        return productRepository.findByName(productDto.getProductName());
    }
}
