package multishop.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Klasa tabeli zamówień
 */
@Entity
@Table(name = "orders")
@Data
public class Order implements Serializable {

    private static final long serialVersionUID = 5363207240207295141L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "order_number", nullable = false)
    private Integer orderNumber;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "client_id"))
    private Client client;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shopping_cart_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "shopping_cart_id"))
    private ShoppingCart shoppingCart;
    @Column(name = "is_send", nullable = false)
    private Boolean isSend;
    @Column(name = "order_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date orderDate;
    @Column(name = "send_order_date")
    @Temporal(TemporalType.DATE)
    private Date sendOrderDate;
}