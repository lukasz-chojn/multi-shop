package multishop.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Klasa tabeli asortymentów
 */
@Entity
@Table(name = "assortments")
@Data
public class Assortment implements Serializable {

    private static final long serialVersionUID = 1155703072526581849L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "quantity", nullable = false)
    private Integer quantity;
    @Column(name = "sold_amount")
    private Integer soldAmount;
}
