package multishop.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Klasa tabeli klientów
 */
@Entity
@Table(name = "clients")
@Data
public class Client implements Serializable {

    private static final long serialVersionUID = 2933044748332854349L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @Column(name = "client_name", nullable = false)
    private String name;
    @Column(name = "client_surname", nullable = false)
    private String surname;
    @Column(name = "client_email", nullable = false)
    private String email;
    @Column(name = "client_address", nullable = false)
    private String clientAddress;
    @Column(name = "client_delivery_address", nullable = false)
    private String deliveryAddress;
}
