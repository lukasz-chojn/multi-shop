package multishop.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

/**
 * Klasa tabeli koszyka
 */
@Entity
@Table(name = "shopping_carts")
@Data
public class ShoppingCart implements Serializable {

    private static final long serialVersionUID = -5391699057153879759L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "client_id", referencedColumnName = "id", foreignKey = @ForeignKey(name = "client_id"))
    private Client client;
    @ManyToMany(cascade = CascadeType.ALL)
    private List<Product> products;
    @Column(name = "sum_prices", precision = 6, scale = 2)
    private BigDecimal sumPrices;
}
