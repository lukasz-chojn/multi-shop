package multishop.repository;

import multishop.model.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Klasa interfejsu produktów
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

    /**
     * wyszukuje listę produktów na podstawie fragmentu słowa
     * @param word słowo
     * @return lista produktów
     */
    List<Product> findByNameContaining(String word);

    /**
     * wyszukuje produkt po jego nazwie
     * @param productName nazwa produktu
     * @return produkt
     */
    Product findByName(String productName);
}
