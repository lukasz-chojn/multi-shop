package multishop.repository;

import multishop.model.Client;
import multishop.model.ShoppingCart;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Klasa interfejsu koszyka
 */
@Repository
public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Integer> {

    /**
     * wyszukuje konkretny koszyk dla klienta po id klienta
     * @param clientId id klienta
     * @return koszyk klienta
     */
    ShoppingCart findByClient(Client clientId);
}
