package multishop.repository;

import multishop.model.Client;
import multishop.model.Order;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Klasa interfejsu zamówień
 */
@Repository
public interface OrderRepository extends CrudRepository<Order, Integer> {

    /**
     * wyszukuję listę zamówień po id klienta
     * @param clientId id klienta
     * @return lista zamówień
     */
    List<Order> findAllByClientId(Integer clientId);

    /**
     * wyszukuje konkretne zamówienie
     * @param orderNumber numer zamówienia
     * @return zamówienie o konkretnym numerze
     */
    Order findByOrderNumber(Integer orderNumber);

    /**
     * wyszukuję listę zamówień po dacie zamówienia i id klienta
     * @param orderDate data zamówienia
     * @return lista zamówień
     */
    List<Order> findAllByOrderDateOrderByOrderDateDesc(Date orderDate);

    /**
     * wyszukuję listę zamówień po dacie wysyłki i id klienta
     * @param sendOrderDate data wysyłki
     * @return lista zamówień
     */
    List<Order> findAllBySendOrderDateOrderBySendOrderDateDesc(Date sendOrderDate);

    /**
     * zwraca istniejące zamówienie klienta, jeśli niewysłane
     * @param clientId id klienta
     * @return zamówienie
     */
    @Query(value = "SELECT * FROM orders WHERE client_id = ?1 AND is_send = false", nativeQuery = true)
    Order findByClientId(Client clientId);

    /**
     * ustawia datę wysyłki zamówienia
     * @param sendOrderDate data wysyłki
     */
    @Transactional
    @Modifying
    @Query(value = "UPDATE orders SET send_order_date = ?1, is_send = true WHERE is_send = false AND send_order_date is null", nativeQuery = true)
    void setSendOrderDate(Date sendOrderDate, List<Order> orderList);
}
