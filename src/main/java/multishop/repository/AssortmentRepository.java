package multishop.repository;

import multishop.model.*;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Klasa interfejsu asortymentów
 */
@Repository
public interface AssortmentRepository extends CrudRepository<Assortment, Integer> {

    /**
     * zwraca ilość produktu dla podanego productId
     * @param productId id produktu
     * @return ilość produktu
     */
    @Query(value = "SELECT quantity FROM assortments WHERE id in (SELECT amount_id FROM products WHERE products.id = ?1)", nativeQuery = true)
    Integer getQuantityOfProduct(Integer productId);

    /**
     * aktualizuje ilość produktu w tabeli asortymentów
     * @param productId id produktu
     * @param quantity ilość produktu
     */
    @Modifying
    @Query(value = "UPDATE assortments SET quantity = ?2 WHERE id in (SELECT amount_id FROM products WHERE products.id = ?1)", nativeQuery = true)
    void updateQuantity(Integer productId, Integer quantity);

    /**
     * aktualizuje ilość sprzedanych produktów
     * @param productId id produktu
     * @param quantity ilość
     */
    @Modifying
    @Query(value = "UPDATE assortments SET sold_amount = ?2 WHERE id in (SELECT amount_id FROM products WHERE products.id = ?1)", nativeQuery = true)
    void updateSellQuantity(Integer productId, Integer quantity);
}
