package multishop.repository;

import multishop.model.Client;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Klasa interfejsu klientów
 */
@Repository
public interface ClientRepository extends CrudRepository<Client, Integer> {

    /**
     * wyszukuje klientów na podstawie zadanych parametrów
     * @param clientAddress adres klienta
     * @param deliveryAddress adres dostawy
     * @return lista klientów
     */
    List<Client> findByClientAddressOrDeliveryAddress(String clientAddress, String deliveryAddress);

    /**
     * wyszukuje klientów na podstawie zadanych parametrów
     * @param name imię
     * @param surname nazwisko
     * @return lista klientów
     */
    List<Client> findByNameAndSurname(String name, String surname);

    /**
     * wyszukuje klientów na podstawie zadanych parametrów
     * @param name imię
     * @param surname nazwisko
     * @param address adres
     * @return klient
     */
    Client findByNameAndSurnameAndClientAddress(String name, String surname, String address);

    /**
     * aktualizuje adres klienta na podstawie parametrów
     * @param clientId id klienta
     * @param oldAddress stary adres
     * @param newAddress nowy adres
     */
    @Modifying
    @Transactional
    @Query(value = "UPDATE clients SET client_address = ?3 WHERE id = ?1 AND client_address = ?2", nativeQuery = true)
    void updateClientAddress(Integer clientId, String oldAddress, String newAddress);
}
