package multishop.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Klasa konfiguracyjna Swagger'a
 */
@Configuration
@EnableSwagger2
@Slf4j
public class SwaggerConfig {

    @Bean
    public Docket api() {
        log.info("Uruchomiono Swaggera");
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("multishop.controller"))
                .paths(PathSelectors.regex("/.*"))
                .build().apiInfo(about());

    }

    private ApiInfo about() {
        return new ApiInfoBuilder()
                .title("Multi shop")
                .description("Multi shop.\n" +
                        "Application created for educational purposes. The application is written in Java 11 using Spring Boot, Web, Security.\n" +
                        "Data is saved using the RabbitMQ mechanism to the PostgreSQL database. The application contains unit tests")
                .version("1.0")
                .build();
    }
}
