package multishop.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Klasa konfiguracyjna dostępu do bazy danych
 */
@Configuration
@PropertySource("classpath:/properties/db.properties")
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = {"multishop"}, entityManagerFactoryRef = "localContainerEntityManagerFactoryBean")
@Slf4j
public class DatabaseConfig {

    @Value("${database.driver}")
    private String databaseDriver;
    @Value("${database.dialect}")
    private String databaseDialect;
    @Value("${database.url}")
    private String databaseUrl;
    @Value("${database.username}")
    private String databaseUsername;
    @Value("${database.password}")
    private String databasePassword;
    @Value("${database.creation.mode}")
    private String databaseCreationMode;

    @Bean
    public DriverManagerDataSource createConnection() {
        log.info("Konfiguracja połączenia do bazy danych");
        DriverManagerDataSource driver = new DriverManagerDataSource();
        driver.setDriverClassName(databaseDriver);
        driver.setUrl(databaseUrl);
        driver.setUsername(databaseUsername);
        driver.setPassword(databasePassword);
        return driver;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean() {
        log.info("Konfiguracja managera encji");
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(createConnection());
        entityManagerFactory.setJpaVendorAdapter(jpaVendorAdapter());
        entityManagerFactory.setPackagesToScan("multishop");
        entityManagerFactory.setPersistenceUnitName("multi-shop");
        entityManagerFactory.setJpaProperties(properties());
        return entityManagerFactory;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        log.info("Podnoszę bean'a " + JpaVendorAdapter.class.getSimpleName());
       HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
       hibernateJpaVendorAdapter.setGenerateDdl(true);
       hibernateJpaVendorAdapter.setShowSql(true);
       return hibernateJpaVendorAdapter;
    }

    @Bean
    public Properties properties() {
        log.info("Konfiguracja parametrów bazy danych");
        Properties properties = new Properties();
        properties.put("hibernate.show_sql", "true");
        properties.put("hibernate.hbm2ddl.auto", databaseCreationMode);
        properties.put("hibernate.dialect", databaseDialect);
        properties.put("hibernate.connection.useUnicode", "true");
        properties.put("hibernate.connection.characterEncoding", StandardCharsets.UTF_8);
        properties.put("hibernate.connection.charSet", StandardCharsets.UTF_8);
        return properties;
    }
}
