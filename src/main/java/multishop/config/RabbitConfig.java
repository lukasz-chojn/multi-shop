package multishop.config;

import lombok.extern.slf4j.Slf4j;
import multishop.config.constants.RabbitConstant;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * Klasa konfiguracyjna menedżera rabbitMQ
 */
@Configuration
@PropertySource("classpath:properties/rabbit.properties")
@EnableRabbit
@Slf4j
public class RabbitConfig {

    @Value("${rabbitmq.host}")
    private String host;
    @Value("${rabbitmq.port}")
    private Integer port;
    @Value("${rabbitmq.username}")
    private String username;
    @Value("${rabbitmq.password}")
    private String password;

    @Bean
    public ConnectionFactory rabbitConnection() {
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory();
        log.info("Konfiguracja bean'a do połączenia z rabbitmq");
        connectionFactory.setHost(host);
        connectionFactory.setPort(port);
        connectionFactory.setUsername(username);
        connectionFactory.setPassword(password);

        return connectionFactory;
    }

    @Bean
    public RabbitTemplate rabbitTemplate() {
        log.info("Konfiguracja " + RabbitTemplate.class.getSimpleName());
        RabbitTemplate template = new RabbitTemplate();
        template.setMessageConverter(jackson2JsonMessageConverter());
        template.setEncoding("UTF-8");
        template.setConnectionFactory(rabbitConnection());
        return template;
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        log.info("Tworzenie bean'a " + Jackson2JsonMessageConverter.class.getSimpleName());
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public RabbitAdmin rabbitAdmin() {
        log.info("Tworzenie bean'a " + RabbitAdmin.class.getSimpleName());
        RabbitAdmin rabbitAdmin = new RabbitAdmin(rabbitConnection());

        log.info("Konfiguracja kolejek");
        rabbitAdmin.declareQueue(addProductQueue());

        log.info("Konfiguracja punktów typu " + TopicExchange.class.getSimpleName());
        rabbitAdmin.declareExchange(addExchange());

        log.info("Łączenie kolejek");
        rabbitAdmin.declareBinding(BindingBuilder
                .bind(addProductQueue())
                .to(addExchange())
                .with(RabbitConstant.ROUTING_KEY_ADD_PRODUCT));

        return rabbitAdmin;
    }

    private Queue addProductQueue() {
        return new Queue(RabbitConstant.ADD_PRODUCT, true, false, false);
    }

    private TopicExchange addExchange() {
        return new TopicExchange(RabbitConstant.ADD_EXCHANGE, true, false);
    }
}
