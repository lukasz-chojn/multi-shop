package multishop.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Klasa konfiguracyjna aplikacji
 */
@Configuration
@Slf4j
public class AppConfig implements WebMvcConfigurer {

    @Bean
    public ViewResolver viewResolver() {
        log.info("Podnoszę bean'a " + InternalResourceViewResolver.class.getSimpleName());
        InternalResourceViewResolver viewResolverRegistry = new InternalResourceViewResolver();
        viewResolverRegistry.setCache(false);
        return viewResolverRegistry;
    }

    @Bean
    public ReloadableResourceBundleMessageSource resourceBundleMessageSource() {
        log.info("Podnoszę bean'a " + ReloadableResourceBundleMessageSource.class.getSimpleName());
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasenames(
                "classpath:/properties/messages/messages_assortment",
                "classpath:/properties/messages/messages_client",
                "classpath:/properties/messages/messages_mail",
                "classpath:/properties/messages/messages_order",
                "classpath:/properties/messages/messages_product",
                "classpath:/properties/messages/messages_shopping_cart");
        messageSource.setAlwaysUseMessageFormat(true);
        messageSource.setDefaultEncoding("Windows-1250");
        return messageSource;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("Konfiguracja danych statycznych");
        registry.addResourceHandler("/resources/**").addResourceLocations("classpath:resources/");
        registry.addResourceHandler("/**").addResourceLocations("classpath:static/", "classpath:static/css/", "classpath:static/js/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("/webjars/");
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:META-INF/resources/");
    }
}
