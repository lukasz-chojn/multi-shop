package multishop.config;

import lombok.extern.slf4j.*;
import org.springframework.context.annotation.*;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Klasa konfiguracji zabezpieczeń w aplikacji
 */
@Configuration
@EnableWebSecurity
@Slf4j
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        log.info("Konfiguracja zabezpieczeń");
        http
                .authorizeRequests()
                .antMatchers("/**").permitAll()
                .and()
                .cors().disable()
                .csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) {
        log.info("Odblokowanie danych statycznych");
        web.ignoring()
                .antMatchers("/css/**", "/js/**", "/webjars/**");
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        log.info("Tworzenie użytkowników");
        auth
                .inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("admin123")).roles("ADMIN")
                .and()
                .withUser("user").password(passwordEncoder().encode("user123")).roles("USER")
                .and()
                .withUser("guest").password(passwordEncoder().encode("guest123")).roles("ANONYMOUS");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        log.info("Podnoszę enkoder haseł");
        return new BCryptPasswordEncoder();
    }
}
