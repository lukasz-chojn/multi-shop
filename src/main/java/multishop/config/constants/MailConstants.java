package multishop.config.constants;

/**
 * Stałe do wysyłki maila
 */
public class MailConstants {

    private MailConstants() {
    }

    public static final String FROM_ADDRESS = "lukasz.chojnowski@interia.eu";
}
