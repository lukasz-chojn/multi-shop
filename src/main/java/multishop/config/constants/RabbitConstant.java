package multishop.config.constants;

/**
 * Stałe konfiguracyjne kolejek dla RabbitMQ
 */
public class RabbitConstant {

    private RabbitConstant() {
    }

    public static final String ADD_PRODUCT = "command.addProduct";
    public static final String ADD_ASSORTMENT = "command.addAssortment";
    public static final String ADD_EXCHANGE = "addExchange";
    public static final String ROUTING_KEY_ADD_PRODUCT = "routing.key.addProduct";
}
