package multishop.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

/**
 * Klasa konfiguracyjna wysyłki maila
 */
@Configuration
@PropertySource("classpath:/properties/mail.properties")
@Slf4j
public class MailConfig {

    @Value("${mail.host}")
    private String mailHost;
    @Value("${mail.port}")
    private Integer mailPort;
    @Value("${mail.username}")
    private String mailUsername;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.protocol}")
    private String mailProtocol;
    @Value("${mail.smtp.auth}")
    private Boolean mailAuth;
    @Value("${mail.encoding}")
    private String mailEncoding;
    @Value("${mail.starttls}")
    private Boolean mailStartTlsEnabled;

    @Bean
    public JavaMailSender javaMailSender() {
        log.info("Konfiguracja wysyłki maila");
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailHost);
        mailSender.setPort(mailPort);

        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        mailSender.setDefaultEncoding(mailEncoding);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", mailProtocol);
        props.put("mail.smtp.auth", mailAuth);
        props.put("mail.smtp.starttls.enable", mailStartTlsEnabled);
        props.put("mail.smtp.ssl.trust", mailHost);

        return mailSender;
    }
}
