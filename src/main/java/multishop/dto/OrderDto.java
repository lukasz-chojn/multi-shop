package multishop.dto;

import lombok.Data;
import multishop.model.Client;

import javax.validation.constraints.NotNull;

/**
 * Klasa pomocnicza dla kontrolera zamówień
 */
@Data
public class OrderDto {

    @NotNull
    private Client client;
}
