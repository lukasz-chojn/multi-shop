package multishop.dto;

import lombok.Data;
import multishop.model.Client;

import javax.validation.constraints.*;

/**
 * Klasa pomocnicza dla kontrolera klientów
 */
@Data
public class ClientDto {

    public static final String STRING = "\\.";

    @NotEmpty(message = "Adres klienta nie może być pusty")
    private String clientAddress;
    @NotEmpty(message = "Adres dostawy nie może być pusty")
    private String deliveryAddress;
    @NotEmpty(message = "Email nie może być pusty")
    @Pattern(regexp = "^([A-Za-z0-9_" + STRING + "]+@+[A-Za-z0-9]+\\.[A-Za-z]{2,4})$", message = "Błędny format adresu email")
    private String email;
    @NotEmpty(message = "Imię nie może być puste")
    private String name;
    @NotEmpty(message = "Nazwisko nie może być puste")
    private String surname;
    @NotNull(message = "Obiekt nie może być pusty")
    private Client client;

    @NotEmpty(message = "Nowe dane adresowe nie mogą być puste")
    private String newAddress;
}
