package multishop.dto;

import lombok.Data;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Klasa pomocnicza dla kontrolera produktów
 */
@Data
public class ProductDto {

    @NotEmpty(message = "Nazwa produktu nie może być pusta")
    private String productName;
    @NotNull(message = "Cena nie może być pusta")
    @DecimalMin(value = "1.0", message = "Cena nie może być niższa od 1 PLN")
    private BigDecimal price;
    @NotNull(message = "Ilość produktu nie może być pusta")
    @Min(value = 0, message = "Ilość musi być większa lub równa 0")
    private Integer quantity;
}
