package multishop.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Klasa pomocnicza do prezentacji koszyka dla klienta
 */
@Data
public class SellProductListDto {

    private String productName;
    private Integer sellAmount;
    private BigDecimal shoppingCartPrice;
}
