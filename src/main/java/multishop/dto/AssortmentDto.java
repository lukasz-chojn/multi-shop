package multishop.dto;

import lombok.Data;
import multishop.model.Product;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Klasa pomocnicza dla kontrolera asortymentów
 */
@Data
public class AssortmentDto {

    @NotNull
    private Product product;
    @NotNull
    @Min(value = 0, message = "Ilość musi być większa lub równa 0")
    private Integer quantity;
}
