package multishop.dto;

import lombok.*;
import multishop.model.*;

import java.math.*;
import java.util.*;

/**
 * Klasa pomocnicza zwracająca listę produktów i ich łączną wartość
 */
@Data
public class ShoppingCartClientDto {

    private List<Product> productList;
    private BigDecimal totalPriceOfProducts;
}
