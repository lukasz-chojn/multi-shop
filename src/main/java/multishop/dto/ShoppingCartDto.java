package multishop.dto;

import lombok.Data;
import multishop.model.Client;
import multishop.model.Product;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Klasa pomocnicza dla kontrolera koszyka zakupów
 */
@Data
public class ShoppingCartDto {

    @NotNull
    private Client client;
    @NotNull
    private List<Product> productList;
}
