package multishop.service;

import lombok.extern.slf4j.Slf4j;
import multishop.config.MailConfig;
import multishop.config.constants.MailConstants;
import multishop.model.Client;
import multishop.model.Order;
import multishop.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 * Klasa odpowiadająca za wysyłkę maila do klienta
 */
@Service
@Slf4j
public class SendMailService {

    private MailConfig mailConfig;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    public void setMailConfig(MailConfig mailConfig) {
        this.mailConfig = mailConfig;
    }

    @Autowired
    public void setResourceBundleMessageSource(ReloadableResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    public void sendMailAfterOrder(Client client, Order order) throws MessagingException {
        MimeMessage mimeMessage = mailConfig.javaMailSender().createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);

        messageHelper.setFrom(MailConstants.FROM_ADDRESS);
        messageHelper.setTo(client.getEmail());
        messageHelper.setSubject(resourceBundleMessageSource.getMessage("CREATE_ORDER_MAIL_SUBJECT", new Object[]{order.getOrderNumber()}, Locale.getDefault()));
        messageHelper.setText(resourceBundleMessageSource.getMessage("CREATE_ORDER_MAIL_BODY", new Object[]{order.getOrderNumber(), client.getName(), client.getSurname(), client.getClientAddress(), client.getDeliveryAddress()}, Locale.getDefault()));
        log.info(resourceBundleMessageSource.getMessage("CREATE_ORDER_MAIL_BODY", new Object[]{order.getOrderNumber(), client.getName(), client.getSurname(), client.getClientAddress(), client.getDeliveryAddress()}, Locale.getDefault()));
        mailConfig.javaMailSender().send(mimeMessage);
    }

    public void sendMailAfterOrderVerification(Client client, Order order) throws MessagingException {
        MimeMessage mimeMessage = mailConfig.javaMailSender().createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);

        messageHelper.setFrom(MailConstants.FROM_ADDRESS);
        messageHelper.setTo(client.getEmail());
        messageHelper.setSubject(resourceBundleMessageSource.getMessage("CREATE_VERIFICATION_MAIL_SUBJECT", new Object[]{order.getOrderNumber()}, Locale.getDefault()));
        messageHelper.setText(resourceBundleMessageSource.getMessage("CREATE_VERIFICATION_MAIL_BODY", new Object[]{order.getOrderDate(), order.getOrderNumber(),
                order.getShoppingCart().getProducts()
                .stream()
                .map(Product::getName)
                .collect(Collectors.toList()), order.getShoppingCart().getSumPrices(), client.getName(), client.getSurname(), client.getClientAddress(), client.getDeliveryAddress()}, Locale.getDefault()));
        mailConfig.javaMailSender().send(mimeMessage);

        log.info(resourceBundleMessageSource.getMessage("CREATE_VERIFICATION_MAIL_BODY", new Object[]{order.getOrderDate(), order.getOrderNumber(),
                order.getShoppingCart().getProducts()
                        .stream()
                        .map(Product::getName)
                        .collect(Collectors.toList()), order.getShoppingCart().getSumPrices(), client.getName(), client.getSurname(), client.getClientAddress(), client.getDeliveryAddress()}, Locale.getDefault()));
    }
}
