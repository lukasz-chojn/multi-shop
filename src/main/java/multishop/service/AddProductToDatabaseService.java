package multishop.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import multishop.config.constants.RabbitConstant;
import multishop.model.Product;
import multishop.repository.ProductRepository;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Locale;

/**
 * Klasa obsługująca odbiór komunikatów z kolejki
 */
@Service
@Slf4j
public class AddProductToDatabaseService {

    public static final String RECEIVED_MESSAGE_FROM_QUEUE = "Odebrano wiadomość z kolejki ";
    private ProductRepository productRepository;
    private ObjectMapper objectMapper;
    private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Autowired
    public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
        this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
    }

    @RabbitListener(queues = RabbitConstant.ADD_PRODUCT)
    public void addProductToDatabase(Message message, Channel channel) throws IOException {

        String productMessageFromQueue = new String(message.getBody());
        long deliveryTag = message.getMessageProperties().getDeliveryTag();
        Product product = objectMapper.readValue(productMessageFromQueue, Product.class);
        log.info(RECEIVED_MESSAGE_FROM_QUEUE + productMessageFromQueue);
        productRepository.save(product);
        channel.basicAck(deliveryTag, false);
        log.info(reloadableResourceBundleMessageSource.getMessage("ADD_PRODUCT", new Object[]{product.getName(), product.getPrice()}, Locale.getDefault()));
        log.info(reloadableResourceBundleMessageSource.getMessage("ADD_ASSORTMENT", new Object[]{product.getName(), product.getAssortment().getQuantity()}, Locale.getDefault()));
    }
}
