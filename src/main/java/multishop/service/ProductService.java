package multishop.service;

import multishop.model.*;
import multishop.repository.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.*;

import javax.persistence.*;
import java.math.*;
import java.util.*;

/**
 * Klasa wspomagająca walidację koszyka
 */
@Service
public class ProductService {

    private AssortmentRepository assortmentRepository;
    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public void setAssortmentRepository(AssortmentRepository assortmentRepository) {
        this.assortmentRepository = assortmentRepository;
    }

    public Integer availableAmount(Product product, Integer requestedQuantity) {
        int amountProduct = assortmentRepository.getQuantityOfProduct(product.getId());
        int amountAfterSell = amountProduct - requestedQuantity;
        if (amountAfterSell <= 0) {
            return 0;
        }
        return amountAfterSell;
    }

    public BigDecimal totalCosts(Product product, Integer requestedQuantity) {
        BigDecimal itemCost;
        BigDecimal totalCost = BigDecimal.ZERO;
        BigDecimal itemPrice = product.getPrice();
        itemCost = itemPrice.multiply(new BigDecimal(requestedQuantity));
        totalCost = totalCost.add(itemCost);
        return totalCost;
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> getProductsFromShoppingCart(Client client) {
        return entityManager
                .createNativeQuery("SELECT p.name, sc.sum_prices, a.sold_amount\n" +
                        "FROM shopping_carts_products scp\n" +
                        "    JOIN products p on scp.products_id = p.id\n" +
                        "    JOIN assortments a on p.amount_id = a.id\n" +
                        "    JOIN shopping_carts sc on scp.shoppingcart_id = sc.id\n" +
                        "WHERE client_id = ?1")
                .setParameter(1, client)
                .getResultList();
    }
}
