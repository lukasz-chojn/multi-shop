package multishop.controller;

import lombok.extern.slf4j.*;
import multishop.dto.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.util.*;

/**
 * Kontroler do obsługi asortymentu
 */
@RestController
@RequestMapping("/assortments")
@Slf4j
public class AssortmentController {

    private AssortmentHelper assortmentHelper;
    private ProductRepository productRepository;
    private BindingErrorHandler bindingErrorHandler;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    public void setAssortmentRepository(AssortmentHelper assortmentHelper) {
        this.assortmentHelper = assortmentHelper;
    }

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setDtoValidator(BindingErrorHandler bindingErrorHandler) {
        this.bindingErrorHandler = bindingErrorHandler;
    }

    @Autowired
    public void setResourceBundleMessageSource(ReloadableResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    @PutMapping
    public ResponseEntity<String> updateQuantity(@RequestBody(required = false) @Valid AssortmentDto assortmentDto, BindingResult bindingResult) {

        bindingErrorHandler.checkForErrors(bindingResult);

        Product product = productRepository.findByName(assortmentDto.getProduct().getName());

        if (product == null) {
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage("NOT_FOUND", new Object[]{}, Locale.getDefault()), HttpStatus.BAD_REQUEST);
        }

        int quantity = assortmentDto.getQuantity();

        assortmentHelper.update(product, quantity, 0);

        log.info(resourceBundleMessageSource.getMessage("UPDATE_QUANTITY_ASSORTMENT", new Object[]{assortmentDto.getProduct().getName(), assortmentDto.getQuantity()}, Locale.getDefault()));
        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("UPDATE_QUANTITY_ASSORTMENT", new Object[]{assortmentDto.getProduct().getName(), assortmentDto.getQuantity()}, Locale.getDefault()), HttpStatus.OK);
    }
}
