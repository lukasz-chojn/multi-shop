package multishop.controller;

import lombok.extern.slf4j.*;
import multishop.dto.*;
import multishop.exception.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.service.*;
import multishop.validator.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.math.*;
import java.util.*;

/**
 * Kontroler do obsługi koszyka produktów
 */
@RestController
@RequestMapping("/shopping-carts")
@Slf4j
public class ShoppingCartController {

    private ShoppingCartRepository shoppingCartRepository;
    private ClientRepository clientRepository;
    private ShoppingCartHelper shoppingCartHelper;
    private ProductService productService;
    private BindingErrorHandler bindingErrorHandler;
    private VerifyEmptyList verifyEmptyList;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    public void setShoppingCartRepository(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Autowired
    public void setShoppingCartHelper(ShoppingCartHelper shoppingCartHelper) {
        this.shoppingCartHelper = shoppingCartHelper;
    }

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @Autowired
    public void setDtoValidator(BindingErrorHandler bindingErrorHandler) {
        this.bindingErrorHandler = bindingErrorHandler;
    }

    @Autowired
    public void setVerifyEmptyList(VerifyEmptyList verifyEmptyList) {
        this.verifyEmptyList = verifyEmptyList;
    }

    @Autowired
    public void setResourceBundleMessageSource(ReloadableResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    @PostMapping
    public ResponseEntity<String> addToShoppingCart(@RequestBody(required = false) @Valid ShoppingCartDto shoppingCartDto,
                                                    BindingResult bindingResult) {
        ShoppingCart shoppingCart = new ShoppingCart();

        bindingErrorHandler.checkForErrors(bindingResult);

        Client client = getClient(shoppingCartDto.getClient().getName(), shoppingCartDto.getClient().getSurname(), shoppingCartDto.getClient().getClientAddress());

        if (ObjectUtils.isEmpty(client)) {
            log.error(resourceBundleMessageSource.getMessage("ERROR_FINDING_CLIENT", new Object[]{client}, Locale.getDefault()));
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage("ERROR_FINDING_CLIENT", new Object[]{client}, Locale.getDefault()), HttpStatus.NOT_FOUND);
        }

        ShoppingCart existingShoppingCart = shoppingCartRepository.findByClient(client);
        ShoppingCartClientDto shoppingCartClientDto = shoppingCartHelper.totalCostsShoppingCart(shoppingCartDto);

        verifyEmptyList.verifyEmptyList(shoppingCartClientDto.getProductList());

        if (existingShoppingCart != null) {
            existingShoppingCart.setProducts(shoppingCartClientDto.getProductList());
            existingShoppingCart.setSumPrices(shoppingCartClientDto.getTotalPriceOfProducts());
            shoppingCartRepository.save(existingShoppingCart);

        } else {
            shoppingCart.setClient(client);
            shoppingCart.setProducts(shoppingCartClientDto.getProductList());
            shoppingCart.setSumPrices(shoppingCartClientDto.getTotalPriceOfProducts());
            shoppingCartRepository.save(shoppingCart);
        }

        log.info(resourceBundleMessageSource.getMessage("PRODUCT_ADDED_TO_SHOPPING_CART", new Object[]{}, Locale.getDefault()));
        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("PRODUCT_ADDED_TO_SHOPPING_CART", new Object[]{}, Locale.getDefault()), HttpStatus.CREATED);
    }

    @GetMapping
    public List<SellProductListDto> getProductsFromShoppingCart(@RequestParam("clientName") String clientName,
                                                                @RequestParam("clientSurname") String clientSurname,
                                                                @RequestParam("clientAddress") String clientAddress) {

        Client client = getClient(clientName, clientSurname, clientAddress);

        if (ObjectUtils.isEmpty(clientName) ||
                ObjectUtils.isEmpty(clientSurname) ||
                ObjectUtils.isEmpty(clientAddress)) {

            resourceBundleMessageSource.getMessage("EMPTY_SHOPPING_CART", new Object[]{}, Locale.getDefault());
        }

        List<SellProductListDto> products = new ArrayList<>();

        for (Object[] product : productService.getProductsFromShoppingCart(client)) {
            SellProductListDto p = new SellProductListDto();
            p.setProductName((String) product[0]);
            p.setShoppingCartPrice((BigDecimal) product[1]);
            p.setSellAmount((Integer) product[2]);
            products.add(p);
        }

        return products;
    }

    private Client getClient(String name, String surname, String clientAddress) {
        return clientRepository.findByNameAndSurnameAndClientAddress(
                name,
                surname,
                clientAddress
        );
    }

    protected void verifyEmptyList(List<Product> productList) {
        verifyEmptyList.verifyEmptyList(productList);
    }
}
