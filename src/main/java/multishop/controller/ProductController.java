package multishop.controller;

import com.fasterxml.jackson.core.*;
import com.google.common.collect.*;
import lombok.extern.slf4j.*;
import multishop.config.constants.*;
import multishop.dto.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.util.*;

/**
 * Kontroler do obsługi produktów
 */
@RestController
@RequestMapping("/products")
@Slf4j
public class ProductController {

    private ProductHelper productHelper;
    private ProductRepository productRepository;
    private BindingErrorHandler bindingErrorHandler;
    private MessageBuilderHelper messageBuilderHelper;
    private ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource;
    public static final String NOT_FOUND_PRODUCT = "NOT_FOUND_PRODUCT";

    @Autowired
    public void setProductHelper(ProductHelper productHelper) {
        this.productHelper = productHelper;
    }

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Autowired
    public void setDtoValidator(BindingErrorHandler bindingErrorHandler) {
        this.bindingErrorHandler = bindingErrorHandler;
    }

    @Autowired
    public void setMessageBuilderHelper(MessageBuilderHelper messageBuilderHelper) {
        this.messageBuilderHelper = messageBuilderHelper;
    }

    @Autowired
    public void setReloadableResourceBundleMessageSource(ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource) {
        this.reloadableResourceBundleMessageSource = reloadableResourceBundleMessageSource;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addProduct(@RequestBody @Valid ProductDto productDto,
                                        BindingResult bindingResult) throws JsonProcessingException {
        Product product = new Product();
        Assortment assortment = new Assortment();

        bindingErrorHandler.checkForErrors(bindingResult);

        Product searchedProduct = productHelper.getExistingProduct(productDto);
        if (!ObjectUtils.isEmpty(searchedProduct) && searchedProduct.getName().equals(productDto.getProductName())) {
            log.error(reloadableResourceBundleMessageSource.getMessage("PRODUCT_FOUND", new Object[]{searchedProduct.getName(), searchedProduct.getPrice()}, Locale.getDefault()));
            return new ResponseEntity<>(reloadableResourceBundleMessageSource.getMessage("PRODUCT_FOUND", new Object[]{searchedProduct.getName(), searchedProduct.getPrice()}, Locale.getDefault()), HttpStatus.BAD_REQUEST);
        }

        product.setName(productDto.getProductName());
        product.setPrice(productDto.getPrice());
        product.setAssortment(assortment);

        assortment.setQuantity(productDto.getQuantity());

        Message productMessage = messageBuilderHelper.buildMessage(product);
        log.info("Dodano do kolejki " + RabbitConstant.ADD_ASSORTMENT + " komunikat " + productMessage);
        return new ResponseEntity<>(reloadableResourceBundleMessageSource.getMessage("ADD_TO_QUEUE", new Object[]{}, Locale.getDefault()), HttpStatus.CREATED);
    }

    @GetMapping
    public Iterable<Product> getAllProducts() {

        if (Iterables.isEmpty(productRepository.findAll())) {
            log.error(reloadableResourceBundleMessageSource.getMessage(NOT_FOUND_PRODUCT, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }
        return productRepository.findAll();
    }

    @GetMapping(value = "/{word}")
    public Iterable<Product> getByName(@PathVariable("word") String word) {
        if (word.equalsIgnoreCase("")) {
            log.error(reloadableResourceBundleMessageSource.getMessage(NOT_FOUND_PRODUCT, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }
        return productRepository.findByNameContaining(word);
    }
}
