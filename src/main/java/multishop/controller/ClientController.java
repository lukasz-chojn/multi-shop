package multishop.controller;

import lombok.extern.slf4j.*;
import multishop.dto.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.validation.*;
import java.util.*;

/**
 * Kontroler do obsługi klientów
 */
@RestController
@RequestMapping("/clients")
@Slf4j
public class ClientController {

    public static final String ERROR_FINDING_CLIENT = "ERROR_FINDING_CLIENT";

    private ClientRepository clientRepository;
    private BindingErrorHandler bindingErrorHandler;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Autowired
    public void setDtoValidator(BindingErrorHandler bindingErrorHandler) {
        this.bindingErrorHandler = bindingErrorHandler;
    }

    @Autowired
    public void setResourceBundleMessageSource(ReloadableResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    @PostMapping
    public ResponseEntity<String> addClient(@RequestBody @Valid ClientDto addClientDto, BindingResult bindingResult) {
        Client client = new Client();

        bindingErrorHandler.checkForErrors(bindingResult);

        Client searchedClient = clientRepository.findByNameAndSurnameAndClientAddress(addClientDto.getName(), addClientDto.getSurname(), addClientDto.getClientAddress());

        if (!ObjectUtils.isEmpty(searchedClient) && searchedClient.getName().equals(addClientDto.getName())) {
            log.error(resourceBundleMessageSource.getMessage("CLIENT_FOUND", new Object[]{searchedClient.getName(), searchedClient.getSurname(), searchedClient.getClientAddress()}, Locale.getDefault()));
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage("CLIENT_FOUND", new Object[]{searchedClient.getName(), searchedClient.getSurname(), searchedClient.getClientAddress()}, Locale.getDefault()), HttpStatus.BAD_REQUEST);
        }
            client.setClientAddress(addClientDto.getClientAddress());
            client.setDeliveryAddress(addClientDto.getDeliveryAddress());
            client.setEmail(addClientDto.getEmail());
            client.setName(addClientDto.getName());
            client.setSurname(addClientDto.getSurname());
            clientRepository.save(client);

        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("CLIENT_ADDED", new Object[]{}, Locale.getDefault()), HttpStatus.CREATED);
    }

    @GetMapping("/{address}")
    public List<Client> getClientByAddress(@PathVariable("address") String address) {

        if (address.equals("") || clientRepository.findByClientAddressOrDeliveryAddress(address, address).isEmpty()) {
            log.error(resourceBundleMessageSource.getMessage(ERROR_FINDING_CLIENT, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }

        log.info(clientRepository.findByClientAddressOrDeliveryAddress(address, address).toString());
        return clientRepository.findByClientAddressOrDeliveryAddress(address, address);
    }

    @GetMapping("/{name}/{surname}")
    public List<Client> getClientByNameAndSurname(@PathVariable("name") String name, @PathVariable("surname") String surname) {

        if (name.equals("") || surname.equals("") || clientRepository.findByNameAndSurname(name, surname).isEmpty()) {
            log.error(resourceBundleMessageSource.getMessage(ERROR_FINDING_CLIENT, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }

        log.info(clientRepository.findByNameAndSurname(name, surname).toString());
        return clientRepository.findByNameAndSurname(name, surname);
    }

    @PutMapping
    public ResponseEntity<String> updateClientAddress(@RequestBody(required = false) @Valid ClientDto clientDto, BindingResult bindingResult) {

        bindingErrorHandler.checkForErrors(bindingResult);

        Client client = clientRepository
                .findByNameAndSurnameAndClientAddress(
                        clientDto.getClient().getName(),
                        clientDto.getClient().getSurname(),
                        clientDto.getClient().getClientAddress()
                );

        if (ObjectUtils.isEmpty(clientDto) || Objects.isNull(client)) {
            log.error(resourceBundleMessageSource.getMessage(ERROR_FINDING_CLIENT, new Object[]{}, Locale.getDefault()));
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage(ERROR_FINDING_CLIENT, new Object[]{}, Locale.getDefault()), HttpStatus.NOT_FOUND);
        }

        clientRepository.updateClientAddress(client.getId(), client.getClientAddress(), clientDto.getNewAddress());
        log.info(resourceBundleMessageSource.getMessage("CLIENT_ADDRESS_UPDATED", new Object[]{client.getClientAddress(), clientDto.getNewAddress()}, Locale.getDefault()));
        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("CLIENT_ADDRESS_UPDATED", new Object[]{client.getClientAddress(), clientDto.getNewAddress()}, Locale.getDefault()), HttpStatus.OK);
    }
}
