package multishop.controller;

import com.google.common.collect.*;
import lombok.extern.slf4j.*;
import multishop.dto.*;
import multishop.exception.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.service.*;
import multishop.validator.*;
import org.springframework.beans.factory.annotation.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.util.*;
import org.springframework.validation.*;
import org.springframework.web.bind.annotation.*;

import javax.mail.*;
import javax.validation.*;
import java.text.*;
import java.util.*;

/**
 * Kontroler do obsługi zamówień
 */
@RestController
@RequestMapping("/orders")
@Slf4j
public class OrderController {

    private OrderRepository orderRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ClientRepository clientRepository;
    private SendMailService sendMailService;
    private BindingErrorHandler bindingErrorHandler;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;

    public static final String DATE_PATTERN = "yyyy-MM-dd";
    public static final String ORDERS_NOT_FOUND = "ORDERS_NOT_FOUND";

    @Autowired
    public void setOrderRepository(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Autowired
    public void setShoppingCartRepository(ShoppingCartRepository shoppingCartRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
    }

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Autowired
    public void setSendMailService(SendMailService sendMailService) {
        this.sendMailService = sendMailService;
    }

    @Autowired
    public void setDtoValidator(BindingErrorHandler bindingErrorHandler) {
        this.bindingErrorHandler = bindingErrorHandler;
    }

    @Autowired
    public void setResourceBundleMessageSource(ReloadableResourceBundleMessageSource resourceBundleMessageSource) {
        this.resourceBundleMessageSource = resourceBundleMessageSource;
    }

    @PostMapping
    public ResponseEntity<String> addOrder(@RequestBody(required = false) @Valid OrderDto orderDto, BindingResult bindingResult) throws ParseException, MessagingException {

        Order order = new Order();
        long now = new Date().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
        String s = simpleDateFormat.format(now);
        Date date = simpleDateFormat.parse(s);

        bindingErrorHandler.checkForErrors(bindingResult);

        Client client = getClient(orderDto);
        ShoppingCart shoppingCart = shoppingCartRepository.findByClient(client);

        if (ObjectUtils.isEmpty(client) || ObjectUtils.isEmpty(shoppingCart)) {
            log.error("Nie znaleziono klienta o id " + client.getId() + " lub " +
                    "Brak koszyka dla klienta o id " + client.getId());

            throw new NotFoundException("Nie znaleziono klienta o id " + client.getId() + " lub " +
                    "Brak koszyka dla klienta o id " + client.getId());
        }

        Order existingOrder = orderRepository.findByClientId(client);

        if (existingOrder == null) {
            int min = 0;
            int max = Integer.MAX_VALUE;

            order.setOrderNumber(new Random().nextInt(max - min) + min);
            order.setIsSend(false);
            order.setOrderDate(date);
            order.setSendOrderDate(null);
            order.setClient(client);
            order.setShoppingCart(shoppingCart);
            orderRepository.save(order);
            sendMailService.sendMailAfterOrder(client, order);
        } else {
            log.error("Twoje zamówienie o numerze " + existingOrder.getOrderNumber() + " czeka na wysyłkę. Złóż kolejne jak wyślemy aktualne.");
            return null;
        }

        log.info(resourceBundleMessageSource.getMessage("ORDER_CREATED", new Object[]{}, Locale.getDefault()));
        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("ORDER_CREATED", new Object[]{}, Locale.getDefault()), HttpStatus.CREATED);
    }

    @GetMapping
    public Iterable<Order> getAllOrders() {
        if (Iterables.isEmpty(orderRepository.findAll())) {
            log.error(resourceBundleMessageSource.getMessage(ORDERS_NOT_FOUND, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }
        return orderRepository.findAll();
    }

    @GetMapping("/{clientId}")
    public List<Order> clientOrders(@PathVariable("clientId") Integer clientId) {
        if (orderRepository.findAllByClientId(clientId).isEmpty()) {
            log.error(resourceBundleMessageSource.getMessage(ORDERS_NOT_FOUND, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }

        return orderRepository.findAllByClientId(clientId);
    }

    @GetMapping("/orderDate")
    public List<Order> ordersByDate(@RequestParam("orderDate") String orderDate) throws ParseException {
        Date date = new SimpleDateFormat(DATE_PATTERN).parse(orderDate);
        if (orderRepository.findAllByOrderDateOrderByOrderDateDesc(date).isEmpty()) {
            log.error(resourceBundleMessageSource.getMessage(ORDERS_NOT_FOUND, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }
        return orderRepository.findAllByOrderDateOrderByOrderDateDesc(date);
    }

    @GetMapping("/sendOrderDate")
    public List<Order> sendOrdersByDate(@RequestParam("sendOrderDate") String sendOrderDate) throws ParseException {
        Date date = new SimpleDateFormat(DATE_PATTERN).parse(sendOrderDate);
        if (orderRepository.findAllBySendOrderDateOrderBySendOrderDateDesc(date).isEmpty()) {
            log.error(resourceBundleMessageSource.getMessage(ORDERS_NOT_FOUND, new Object[]{}, Locale.getDefault()));
            return Collections.emptyList();
        }
        return orderRepository.findAllBySendOrderDateOrderBySendOrderDateDesc(date);
    }

    @PutMapping(value = "/{orderNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> verifyOrder(@PathVariable("orderNumber") Integer orderNumber) throws ParseException, MessagingException {
        long l = new Date().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_PATTERN);
        String format = simpleDateFormat.format(l);
        Date date = simpleDateFormat.parse(format);
        Order order = orderRepository.findByOrderNumber(orderNumber);

        if (ObjectUtils.isEmpty(order)) {
            log.error(resourceBundleMessageSource.getMessage("ORDER_NOT_FOUND", new Object[]{}, Locale.getDefault()));
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage("ORDER_NOT_FOUND", new Object[]{}, Locale.getDefault()), HttpStatus.NOT_FOUND);
        } else if (order.getSendOrderDate() != null) {
            log.error(resourceBundleMessageSource.getMessage("ORDER_SENT", new Object[]{order.getSendOrderDate()}, Locale.getDefault()));
            return new ResponseEntity<>(resourceBundleMessageSource.getMessage("ORDER_SENT", new Object[]{order.getSendOrderDate()}, Locale.getDefault()), HttpStatus.NOT_FOUND);
        }

        orderRepository.setSendOrderDate(date, Collections.singletonList(order));

        sendMailService.sendMailAfterOrderVerification(order.getClient(), order);

        log.info(resourceBundleMessageSource.getMessage("ORDER_SEND", new Object[]{order.getOrderNumber()}, Locale.getDefault()));
        return new ResponseEntity<>(resourceBundleMessageSource.getMessage("ORDER_SEND", new Object[]{order.getOrderNumber()}, Locale.getDefault()), HttpStatus.OK);
    }

    protected Client getClient(@RequestBody(required = false) @Valid OrderDto orderDto) {
        return clientRepository.findByNameAndSurnameAndClientAddress(
                orderDto.getClient().getName(),
                orderDto.getClient().getSurname(),
                orderDto.getClient().getClientAddress());
    }
}
