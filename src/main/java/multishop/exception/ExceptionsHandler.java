package multishop.exception;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

/**
 * kontroler przechwytujący błędy
 */
@RestControllerAdvice
public class ExceptionsHandler {

    public static final String CONTENT_TYPE_TEXT_HTML_CHARSET_UTF_8 = "Content-Type, text/html; charset=UTF-8";

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> notFoundException(NotFoundException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE_TEXT_HTML_CHARSET_UTF_8)
                .body(e.getMessage());
    }

    @ExceptionHandler(EmptyShoppingCartException.class)
    public ResponseEntity<String> emptyShoppingCartException(EmptyShoppingCartException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE_TEXT_HTML_CHARSET_UTF_8)
                .body(e.getMessage());
    }

    @ExceptionHandler(RequestValidationException.class)
    public ResponseEntity<String> dtoValidationError(RequestValidationException e) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .header(CONTENT_TYPE_TEXT_HTML_CHARSET_UTF_8)
                .body(e.getMessage());
    }
}
