package multishop.exception;

import org.springframework.http.*;

/**
 * Klasa wyjątku po weryfikacji obiektu dto zakończonej błędem
 */
public class RequestValidationException extends RuntimeException {

    private static final long serialVersionUID = -3523021266653478051L;

    public RequestValidationException(String message, HttpStatus httpStatus) {
        super(message);
    }
}
