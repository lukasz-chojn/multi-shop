package multishop.exception;

/**
 * Wyjątek informujący o pustym koszyku
 */
public class EmptyShoppingCartException extends RuntimeException {

    private static final long serialVersionUID = 2704059244405285620L;

    public EmptyShoppingCartException(String message) {
        super(message);
    }
}
