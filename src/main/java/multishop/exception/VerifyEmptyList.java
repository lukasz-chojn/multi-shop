package multishop.exception;

import lombok.extern.slf4j.*;
import multishop.model.*;
import org.springframework.stereotype.*;

import java.util.*;

/**
 * Weryfikacja pustego koszyka
 */
@Component
@Slf4j
public class VerifyEmptyList {

    public void verifyEmptyList(List<Product> productList) {
        if (productList == null || productList.isEmpty()) {
            log.error("Koszyk jest pusty");
            throw new EmptyShoppingCartException("Koszyk jest pusty");
        }
    }
}