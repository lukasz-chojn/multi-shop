package multishop.exception;

/**
 * Klasa wyjątku biznesowego nie znalezienia klienta lub koszyka
 */
public class NotFoundException extends RuntimeException {

    private static final long serialVersionUID = 3319549071368410209L;

    public NotFoundException(String message) {
        super(message);
    }
}
