package multishop.controller;

import com.fasterxml.jackson.databind.*;
import multishop.dto.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.service.*;
import multishop.validator.*;
import org.mockito.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;
import org.springframework.test.web.servlet.setup.*;
import org.testng.annotations.*;

import java.math.*;
import java.text.*;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.testng.Assert.*;

public class OrderControllerTest {

    private MockMvc mockMvc;
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private ShoppingCartRepository shoppingCartRepository;
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private SendMailService sendMailService;
    @Mock
    private BindingErrorHandler bindingErrorHandler;
    @InjectMocks
    private OrderController orderController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(orderController).build();
        ReloadableResourceBundleMessageSource resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("classpath:/msg/messages_order");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        orderController.setResourceBundleMessageSource(resourceBundleMessageSource);
    }

    @Test
    public void testAddOrder() throws Exception {
        OrderDto orderDto = orderDto();

        when(clientRepository.findByNameAndSurnameAndClientAddress(
                orderDto.getClient().getName(),
                orderDto.getClient().getSurname(),
                orderDto.getClient().getClientAddress()
        )).thenReturn(client());

        when(shoppingCartRepository.findByClient(orderDto.getClient())).thenReturn(shoppingCart());

        when(orderRepository.findByClientId(orderDto.getClient())).thenReturn(null);

        MockHttpServletRequestBuilder requestBuilder = post("/orders", orderDto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(orderDto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.TEXT_PLAIN_VALUE + ";charset=ISO-8859-1");
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 201);
    }

    @Test
    public void testGetAllOrders() throws Exception {
        when(orderRepository.findAll()).thenReturn(orderIterable());

        MockHttpServletRequestBuilder requestBuilder = get("/orders");
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testClientOrders() throws Exception {
        Order order = order();

        when(orderRepository
                .findAllByClientId(order.getClient().getId()))
                .thenReturn(Collections.singletonList(order()));

        MockHttpServletRequestBuilder requestBuilder = get("/orders/{clientId}", order.getClient().getId());
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.sessionAttr("order", order)
                .param("clientId", String.valueOf(order.getClient().getId()));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testOrdersByDate() throws Exception {
        Order order = order();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
        String formatDate = simpleDateFormat.format(order.getOrderDate());
        Date date = simpleDateFormat.parse(formatDate);

        when(orderRepository.findAllByOrderDateOrderByOrderDateDesc(date))
                .thenReturn(Collections.singletonList(order()));

        MockHttpServletRequestBuilder requestBuilder =
                get("/orders/orderDate", formatDate);
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestBuilder.sessionAttr("order", order)
                .param("orderDate", formatDate);

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testSendOrdersByDate() throws Exception {
        long sendDate = new Date().getTime();
        SimpleDateFormat simpleDateFormat= new SimpleDateFormat("yyyy-MM-dd");
        String formatDate = simpleDateFormat.format(sendDate);
        Date date = simpleDateFormat.parse(formatDate);
        Order order = order();

        when(orderRepository
                .findAllBySendOrderDateOrderBySendOrderDateDesc(date))
                .thenReturn(Collections.singletonList(order()));

        MockHttpServletRequestBuilder requestBuilder =
                get("/orders/sendOrderDate", formatDate);
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestBuilder.sessionAttr("order", order)
        .param("sendOrderDate", formatDate);

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testVerifyOrder() throws Exception {
        Order order = order();

        when(orderRepository.findByOrderNumber(anyInt())).thenReturn(order);

        MockHttpServletRequestBuilder requestBuilder = put("/orders/{orderNumber}", order.getOrderNumber());
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestBuilder.sessionAttr("order", order)
                .param("orderNumber", String.valueOf(order.getOrderNumber()));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testGetClient() {
        Client client = client();

        when(clientRepository
                .findByNameAndSurnameAndClientAddress(
                        client.getName(),
                        client.getSurname(),
                        client.getClientAddress()
                )).thenReturn(client);
    }

    private List<Product> products() {
        Product p1 = new Product();
        p1.setId(1);
        p1.setName("p1");
        p1.setPrice(new BigDecimal("5.00"));
        p1.setAssortment(assortment());
        return Collections.singletonList(p1);
    }

    private Assortment assortment() {
        Assortment assortment = new Assortment();
        assortment.setId(1);
        assortment.setQuantity(10);
        assortment.setSoldAmount(null);
        return assortment;
    }

    private Client client() {
        Client client = new Client();
        client.setId(1);
        client.setName("test");
        client.setSurname("testowy");
        client.setEmail("test@test.com");
        client.setClientAddress("adres zamieszkania");
        client.setDeliveryAddress("adres do dostawy");
        return client;
    }

    private ShoppingCart shoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setId(1);
        shoppingCart.setClient(client());
        shoppingCart.setProducts(products());
        shoppingCart.setSumPrices(new BigDecimal("15.00"));
        return shoppingCart;
    }

    private Order order() {
        Order order = new Order();
        order.setId(1);
        order.setOrderNumber(new Random().nextInt());
        order.setOrderDate(new Date());
        order.setIsSend(false);
        order.setSendOrderDate(null);
        order.setClient(client());
        order.setShoppingCart(shoppingCart());
        return order;
    }

    private OrderDto orderDto() {
        OrderDto dto = new OrderDto();
        dto.setClient(client());
        return dto;
    }

    private Iterable<Order> orderIterable() {
        Order order = new Order();
        order.setId(1);
        order.setOrderNumber(new Random().nextInt());
        order.setOrderDate(new Date());
        order.setIsSend(false);
        order.setSendOrderDate(null);
        order.setClient(client());
        order.setShoppingCart(shoppingCart());
        return Collections.singletonList(order);
    }
}