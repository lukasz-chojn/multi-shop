package multishop.controller;

import com.fasterxml.jackson.databind.*;
import multishop.dto.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.mockito.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;
import org.springframework.test.web.servlet.setup.*;
import org.testng.annotations.*;

import java.math.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.testng.Assert.*;

public class AssortmentControllerTest {
    private MockMvc mockMvc;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private AssortmentHelper assortmentHelper;
    @Mock
    private BindingErrorHandler bindingErrorHandler;
    private ReloadableResourceBundleMessageSource resourceBundleMessageSource;
    @InjectMocks
    private AssortmentController assortmentController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(assortmentController).build();
        resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("classpath:/msg/messages_assortment");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        assortmentController.setResourceBundleMessageSource(resourceBundleMessageSource);
    }

    @Test
    void testUpdateQuantity() throws Exception {

        AssortmentDto assortmentDto = new AssortmentDto();
        Product product = product();
        assortmentDto.setQuantity(1);
        assortmentDto.setProduct(product);

        when(productRepository.findByName(product.getName())).thenReturn(product());

        assortmentHelper.update(product, 1, null);
        verify(assortmentHelper).update(product, 1, null);

        MockHttpServletRequestBuilder requestBuilder = put("/assortments", assortmentDto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(assortmentDto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertNotNull(resourceBundleMessageSource);
        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.TEXT_PLAIN_VALUE + ";charset=ISO-8859-1");
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);


    }

    private Product product() {
        Product product = new Product();
        product.setId(1);
        product.setName("test");
        product.setPrice(new BigDecimal("10.00"));
        product.setAssortment(assortment());
        return product;
    }

    private Assortment assortment() {
        Assortment assortment = new Assortment();
        assortment.setId(1);
        assortment.setQuantity(0);
        assortment.setSoldAmount(0);
        return assortment;
    }
}