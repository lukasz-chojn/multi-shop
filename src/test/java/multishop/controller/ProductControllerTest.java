package multishop.controller;

import com.fasterxml.jackson.databind.*;
import com.google.common.collect.*;
import multishop.dto.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.mockito.*;
import org.springframework.amqp.rabbit.core.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;
import org.springframework.test.web.servlet.setup.*;
import org.testng.annotations.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.testng.Assert.*;

public class ProductControllerTest {

    private MockMvc mockMvc;
    @Mock
    private ProductHelper productHelper;
    @Mock
    private MessageBuilderHelper messageBuilderHelper;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private RabbitTemplate rabbitTemplate;
    @Mock
    private BindingErrorHandler bindingErrorHandler;
    @InjectMocks
    private ProductController productController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build();
        ReloadableResourceBundleMessageSource resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("classpath:/msg/messages_product");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        productController.setReloadableResourceBundleMessageSource(resourceBundleMessageSource);
    }

    @Test
    public void testAddProduct() throws Exception {
        ProductDto productDto = productDto();
        Product product = new Product();
        Assortment assortment = new Assortment();

        product.setId(1);
        product.setName(productDto.getProductName());
        product.setPrice(productDto.getPrice());
        product.setAssortment(assortment);

        assortment.setId(1);
        assortment.setQuantity(productDto.getQuantity());
        assortment.setSoldAmount(null);

        when(productHelper.getExistingProduct(productDto)).thenReturn(null);

        MockHttpServletRequestBuilder requestBuilder = post("/products", productDto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(productDto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 201);
    }

    @Test
    public void testGetAllProducts() throws Exception {
        Iterable<Product> products = products();

        when(productRepository.findAll()).thenReturn(products);

        MockHttpServletRequestBuilder requestBuilder = get("/products");
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    @Test
    public void testGetByName() throws Exception {
        Iterable<Product> products = products();
        Product p = Iterables.get(products, 0);

        when(productRepository.findByName(p.getName())).thenReturn(p);

        MockHttpServletRequestBuilder requestBuilder = get("/products/by-name/");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        requestBuilder.requestAttr("products", products)
                .param("word", p.getName());

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    private Iterable<Product> products() {
        Product p1 = new Product();
        Product p2 = new Product();

        p1.setId(1);
        p1.setName("p1");
        p1.setPrice(new BigDecimal("5.00"));

        p2.setId(2);
        p2.setName("p2");
        p2.setPrice(new BigDecimal("10.00"));

        return Arrays.asList(p1, p2);
    }

    private ProductDto productDto() {
        ProductDto productDto = new ProductDto();
        productDto.setProductName("test");
        productDto.setPrice(new BigDecimal("25.62"));
        productDto.setQuantity(5);
        return productDto;
    }
}