package multishop.controller;

import com.fasterxml.jackson.databind.*;
import multishop.dto.*;
import multishop.exception.*;
import multishop.helper.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.service.*;
import multishop.validator.*;
import org.mockito.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;
import org.springframework.test.web.servlet.setup.*;
import org.testng.annotations.*;

import java.math.*;
import java.util.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.testng.Assert.*;

public class ShoppingCartControllerTest {

    private MockMvc mockMvc;
    @Mock
    private ShoppingCartRepository shoppingCartRepository;
    @Mock
    private ClientRepository clientRepository;
    @Mock
    private ProductService productService;
    @Mock
    private BindingErrorHandler bindingErrorHandler;
    @Mock
    private ShoppingCartHelper shoppingCartHelper;
    @Mock
    private VerifyEmptyList verifyEmptyList;
    @InjectMocks
    private ShoppingCartController shoppingCartController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(shoppingCartController).build();
        ReloadableResourceBundleMessageSource resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("classpath:/msg/messages_shopping_cart");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        shoppingCartController.setResourceBundleMessageSource(resourceBundleMessageSource);
    }

    @Test
    public void testAddToShoppingCart() throws Exception {
        ShoppingCartDto shoppingCartDto = shoppingCartDto();
        Client client = client();
        ShoppingCart shoppingCart = shoppingCart();
        ShoppingCartClientDto shoppingCartClientDto = shoppingCartClientDto();

        when(clientRepository.findByNameAndSurnameAndClientAddress(
                shoppingCartDto.getClient().getName(),
                shoppingCartDto.getClient().getSurname(),
                shoppingCartDto.getClient().getClientAddress()
        )).thenReturn(client);

        when(shoppingCartRepository.findByClient(client)).thenReturn(shoppingCart);

        when(shoppingCartHelper.totalCostsShoppingCart(shoppingCartDto))
                .thenReturn(shoppingCartClientDto);

        MockHttpServletRequestBuilder requestBuilder = post("/shopping-carts", shoppingCartDto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(shoppingCartDto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 201);
    }

    @Test
    public void testGetProductsFromShoppingCart() throws Exception {
        Client client = client();


        when(clientRepository.findByNameAndSurnameAndClientAddress(
                client.getName(), client.getSurname(), client.getClientAddress()
        )).thenReturn(client);

        MockHttpServletRequestBuilder requestBuilder = get("/shopping-carts");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestBuilder.sessionAttr("client", client)
                .param("clientName", client.getName())
                .param("clientSurname", client.getSurname())
                .param("clientAddress", client.getClientAddress());

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
    }

    private List<Product> products() {
        Product p1 = new Product();
        Product p2 = new Product();
        p1.setId(1);
        p1.setName("p1");
        p1.setPrice(new BigDecimal("5.00"));
        p1.setAssortment(assortment());

        p2.setId(2);
        p2.setName("p2");
        p2.setPrice(new BigDecimal("15.00"));
        p2.setAssortment(assortment());
        return Arrays.asList(p1, p2);
    }

    private Assortment assortment() {
        Assortment assortment = new Assortment();
        assortment.setId(1);
        assortment.setQuantity(10);
        assortment.setSoldAmount(null);
        return assortment;
    }

    private Client client() {
        Client client = new Client();
        client.setId(1);
        client.setName("test");
        client.setSurname("testowy");
        client.setEmail("test@test.com");
        client.setClientAddress("adres zamieszkania");
        client.setDeliveryAddress("adres do dostawy");
        return client;
    }

    private ShoppingCartDto shoppingCartDto() {
        ShoppingCartDto shoppingCartDto = new ShoppingCartDto();
        shoppingCartDto.setClient(client());
        shoppingCartDto.setProductList(products());
        return shoppingCartDto;
    }

    private ShoppingCart shoppingCart() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.setId(1);
        shoppingCart.setClient(client());
        shoppingCart.setProducts(products());
        shoppingCart.setSumPrices(new BigDecimal("20.00"));
        return shoppingCart;
    }

    private ShoppingCartClientDto shoppingCartClientDto() {
        ShoppingCartClientDto shoppingCartClientDto = new ShoppingCartClientDto();
        shoppingCartClientDto.setProductList(products());
        shoppingCartClientDto.setTotalPriceOfProducts(new BigDecimal("20.00"));
        return shoppingCartClientDto;
    }
}