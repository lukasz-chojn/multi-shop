package multishop.controller;

import com.fasterxml.jackson.databind.*;
import multishop.dto.*;
import multishop.model.*;
import multishop.repository.*;
import multishop.validator.*;
import org.mockito.*;
import org.springframework.context.support.*;
import org.springframework.http.*;
import org.springframework.test.web.servlet.*;
import org.springframework.test.web.servlet.request.*;
import org.springframework.test.web.servlet.setup.*;
import org.testng.annotations.*;

import java.util.*;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.testng.Assert.*;

public class ClientControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ClientRepository clientRepository;
    @Mock
    private BindingErrorHandler bindingErrorHandler;
    @InjectMocks
    private ClientController clientController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(clientController).build();
        ReloadableResourceBundleMessageSource resourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        resourceBundleMessageSource.setBasenames("classpath:/msg/messages_client");
        resourceBundleMessageSource.setUseCodeAsDefaultMessage(true);
        clientController.setResourceBundleMessageSource(resourceBundleMessageSource);
    }

    @Test
    public void testAddClient() throws Exception {
        ClientDto dto = addClientDto();

        MockHttpServletRequestBuilder requestBuilder = post("/clients", dto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(dto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.TEXT_PLAIN_VALUE + ";charset=ISO-8859-1");
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 201);
    }

    @Test
    public void testGetClientByAddress() throws Exception {
        Client client = client();

        when(clientRepository.findByClientAddressOrDeliveryAddress(client.getClientAddress(),
                client.getDeliveryAddress()))
                .thenReturn(Collections.singletonList(client));

        MockHttpServletRequestBuilder requestBuilder = get("/clients/{address}", client.getClientAddress());
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED);
        requestBuilder.sessionAttr("client", client)
                .param("address", client.getClientAddress());

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);

    }

    @Test
    public void testGetClientByNameAndSurname() throws Exception {
        Client client = client();

        when(clientRepository.findByNameAndSurname(client.getName(), client.getSurname()))
                .thenReturn(Collections.singletonList(client()));

        MockHttpServletRequestBuilder requestBuilder = get("/clients/by-name-and-surname/");
        requestBuilder.contentType(MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        requestBuilder.sessionAttr("client", client)
        .param("name", client.getName())
        .param("surname", client.getSurname());

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.APPLICATION_JSON_VALUE);
    }

    @Test
    public void testUpdateClientAddress() throws Exception {
        Client client = client();
        ClientDto clientDto = new ClientDto();
        clientDto.setClient(client);
        clientDto.setNewAddress("nowy adres");

        when(clientRepository.
                findByNameAndSurnameAndClientAddress(
                        client.getName(),
                        client.getSurname(),
                        client.getClientAddress())).thenReturn(client());

        MockHttpServletRequestBuilder requestBuilder = put("/clients", clientDto);
        requestBuilder.contentType(MediaType.APPLICATION_JSON_VALUE);
        requestBuilder.content(objectMapper.writeValueAsString(clientDto));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        assertEquals(resultActions.andReturn().getResponse().getContentType(), MediaType.TEXT_PLAIN_VALUE + ";charset=ISO-8859-1");
        assertEquals(resultActions.andReturn().getResponse().getStatus(), 200);
    }

    private Client client() {
        Client client = new Client();
        client.setId(1);
        client.setName("test");
        client.setSurname("testowy");
        client.setEmail("test@test.com");
        client.setClientAddress("adres_zamieszkania");
        client.setDeliveryAddress("adres_do_dostawy");
        return client;
    }

    private ClientDto addClientDto() {
        ClientDto dto = new ClientDto();
        dto.setName("test");
        dto.setSurname("testowy");
        dto.setEmail("test@test.com");
        dto.setClientAddress("adres zamieszkania");
        dto.setDeliveryAddress("adres dostawy");
        return dto;
    }
}