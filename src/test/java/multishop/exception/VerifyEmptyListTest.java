package multishop.exception;

import multishop.model.*;
import org.mockito.*;
import org.testng.annotations.*;

import java.util.*;

import static org.mockito.Mockito.doThrow;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

public class VerifyEmptyListTest {

    @Mock
    private VerifyEmptyList verifyEmptyList;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void testVerifyEmptyList() throws EmptyShoppingCartException {
        List<Product> productList = new ArrayList<>();

        doThrow(new EmptyShoppingCartException("Koszyk jest pusty"))
                .when(verifyEmptyList).verifyEmptyList(productList);

        assertEquals(productList.size(), 0);
        assertThrows(EmptyShoppingCartException.class, () -> verifyEmptyList.verifyEmptyList(productList));
    }
}