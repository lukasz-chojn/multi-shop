package multishop.service;

import multishop.model.*;
import multishop.repository.*;
import org.mockito.*;
import org.testng.annotations.*;

import java.math.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ProductServiceTest {

    @Mock
    private AssortmentRepository assortmentRepository;
    @InjectMocks
    private ProductService productService;

    @BeforeMethod
    void setUp() {
        initMocks(this);
    }

    @Test(dataProvider = "availableAmountTestData")
    public void availableAmount(int requestedQuantity, int amountAfterSell) {
        Product product = getProduct();

        when(assortmentRepository.getQuantityOfProduct(product.getId())).thenReturn(10);
        int amount = productService.availableAmount(product, requestedQuantity);

        assertEquals(amountAfterSell, amount);
    }

    @Test(dataProvider = "totalCostsTestData")
    public void totalCosts(int requestedQuantity, BigDecimal expectedPrice) {
        BigDecimal totalCost = BigDecimal.ZERO;
        Product product = getProduct();
        BigDecimal cost = productService.totalCosts(product, requestedQuantity);
        totalCost = totalCost.add(cost);

        assertEquals(expectedPrice, totalCost);
    }

    @DataProvider
    public static Object[][] availableAmountTestData() {
        return new Object[][]{
                {5, 5},
                {20, 0}
        };
    }

    @DataProvider
    public static Object[][] totalCostsTestData() {
        return new Object[][]{
                {2, new BigDecimal("30.00")},
                {5, new BigDecimal("75.00")},
                {0, new BigDecimal("0.00")}
        };
    }

    private Product getProduct() {
        Product p = new Product();
        p.setId(1);
        p.setName("test");
        p.setPrice(new BigDecimal("15.00"));
        return p;
    }
}